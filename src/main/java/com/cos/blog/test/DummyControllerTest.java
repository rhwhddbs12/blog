package com.cos.blog.test;

import com.cos.blog.model.RoleType;
import com.cos.blog.model.User;
import com.cos.blog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.EmptyStackException;
import java.util.List;
import java.util.function.Supplier;

@RestController //리턴이 html 파일이 아니라 데이터 타입
public class DummyControllerTest {

    @Autowired //의존성 주입(DI)
    private UserRepository userRepository;



    //email,password 수정 @RequestBody는 json데이터를 받는데 필요
    @Transactional // 함수 종료 시 자동 commit이 된다.
    @PutMapping("/dummy/user/{id}")
    public User updateUser(@PathVariable int id, @RequestBody User requestUser) { //json데이터를 요청-> JAVA Object로 변환해서 받아줌(Message converter jackson라이브러리가)
        System.out.println("id = " + id);
        System.out.println("requestUser.getPassword() = " + requestUser.getPassword());
        System.out.println("requestUser.getEmail() = " + requestUser.getEmail());

//        requestUser.setId(id);
//        requestUser.setUsername("love");
//        userRepository.save(requestUser);  // 문제는 파라미터를 제외한 값 (자동 스탬프타임 ,default값은) null로 들어감

        //이렇게 하면 User의 모든 정보를 다 가져와서 null값 문제 해결
        User user = userRepository.findById(id).orElseThrow(()->{
            return new IllegalArgumentException("수정에 실패했습니다.");
        });//find by id를 시행하고 못찾으면 orElseThrow를 실행해라
        user.setPassword(requestUser.getPassword());
        user.setEmail(requestUser.getEmail());

        //@Transactional을 사용하면 save를 사용하지 않아도 된다.
        //userRepository.save(user);  //save함수는 id를 전달하지 않으면 insert를 해주고
                                    //save함수는 id를 전달하면 해당 id에 대한 데이터가 있으면 update를 해주고
                                    //save함수는 id를 전달하면 해당 id에 대한 데이터가 없으면 insert를 함
        //더티 체킹
        return user;
    }   
    @DeleteMapping("/dummy/user/{id}")
    public String delete(@PathVariable int id){
        try {
            userRepository.deleteById(id);
        }catch (EmptyResultDataAccessException e){ // Exception으로 하면 다 걸림 얘가 부모 근데 옆엔 자세하게 하기위함
            return"삭제 실패. 해당 아이디는 데이터베이스에 없음.";
        }

        return "삭제되었습니다.id:"+id;
    }



    @GetMapping("/dummy/users")
    public List<User> list() {
        return userRepository.findAll();
    }
    // 한 페이지당 2건에 데이터를 리턴받아 볼 예정   jsp는 원래 우리가 로직으로 정리해야하지만 jPA는 pageable이란 기능이 있음
    @GetMapping("/dummy/user")
    public List<User> pageList(@PageableDefault(size=2,sort="id",direction = Sort.Direction.DESC)Pageable pageable){
        Page<User> paginUser= userRepository.findAll(pageable);

        //pagin이란 클래스가 제공해주는 기능들 아래주석과같이 분기 처리가능
//        if (paginUser.isFirst()) {
//
//        }
        List<User> users = paginUser.getContent();
        return users;
    }

    //{id}주소로 파라미터를 전달 받을 수 있음.
    //ex) localhost:8000/blog/dummy/user/3
    @GetMapping("/dummy/user/{id}")
    public User detail(@PathVariable int id) {
        //user/4을 찾으면 내가 데이터베이스에서 못찾을 경우 user가 null이 됨 그럼 return에 null이 리턴되면 문제가 생길 수 있으니 Optinal로
        // User객체를 감싸서 가져오니 null인지 아닌지 판단해서 return
//       //람다식
//        User user =userRepository.findById(id).orElseThrow(()->{
//            return new IllegalArgumentException("해당 사용자는 없습니다.");
//        });
        User user =userRepository.findById(id).orElseThrow(new Supplier<IllegalArgumentException>() {
            @Override
            public IllegalArgumentException get() {
                return new IllegalArgumentException("해당 유저는 없습니다.id: "+id);
            }
        });
        // 요청 : 웹브라우저
        // user 객체 = 자바 오브젝트
        // 변환 ( 웹브라우저가 이해할 수 있는 데이터) -> json (Gson 라이브러리 사용 자바 obj -> Json으로 )
        // 스프링 부트 = MessageConverter라는 애가 응답시에 자동 작동
        // 만약 자바 오브젝트를 리턴하게되면 MessageConverter가 Jackson이라는 라이브러리를 호출해서
        // user 오브젝트를 json으 변환해서 브라우저에 던져줌
        return user;
    }

    @PostMapping("/dummy/join")
    public String join(User user) {
        System.out.println("id: "+user.getId());
        System.out.println("username: " + user.getUsername());
        System.out.println("pass: " + user.getPassword());
        System.out.println("email: " + user.getEmail());
        System.out.println("role: " + user.getRole());
        System.out.println("createDate: " + user.getCreateDate());

        user.setRole(RoleType.USER); //<-- enum타입으로 디폴트값
        userRepository.save(user);
        return "회원가입이 완료되었어요!";
    }
}
