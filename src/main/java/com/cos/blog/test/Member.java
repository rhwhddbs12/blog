package com.cos.blog.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data//getter & setter
@AllArgsConstructor // final 붙지 않은 전체 생성자 생성
@NoArgsConstructor //빈생성자
//@RequiredArgsConstructor // final 붙은 생성자 생성
public class Member {
    private int id;  // final은 데이터베이스에서 들고온 데이터를 변경할 일이 없으니까
    private String username;
    private String password;
    private String email;

}
