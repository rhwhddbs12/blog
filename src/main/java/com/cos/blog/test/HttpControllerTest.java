package com.cos.blog.test;

import org.springframework.web.bind.annotation.*;

//사용자가 요청 -> 응답(HTML 파일)
//@Controller

//사용자가 요청 -> 응답(data)

@RestController
public class HttpControllerTest {

    private static final String TAG = "HttpControllerTest:";
    @GetMapping("/http/lombok")
    public String lombokTest(){
        Member m = new Member(1, "asdf", "1234", "email");
        System.out.println(TAG+"getter:"+m.getId());
        m.setId(5000);
        System.out.println(TAG+"getter:"+m.getId());
        return "lombok test 완료";
    }
    //@RequestParam -> http://localhost:8080/http/get?id=1  ?id=1 부분(쿼리스트링) 을 받는다
//    @GetMapping("/http/get")
//    public String getTest(@RequestParam int id, @RequestParam String username){
//        return "get 요청:"+id;
//    }

    //@RequestParam -> 처럼 하나하나 말고 여러가지 한번에 처리
    @GetMapping("/http/get")
    public String getTest(Member m){

        return "get 요청 :"+m.getId()+","+m.getUsername()+","+m.getPassword()+","+m.getEmail();
    }
    @PostMapping("/http/post")
    public String postTest(@RequestBody Member m){
        return "post 요청 :"+m.getId()+","+m.getUsername()+","+m.getPassword()+","+m.getEmail();
    }
    @PutMapping("/http/put")
    public String putTest(@RequestBody Member m){
        return "put 요청 :"+m.getId()+","+m.getUsername()+","+m.getPassword()+","+m.getEmail();
    }
    @DeleteMapping("/http/delete")
    public String deleteTest(){
        return "delete 요청";
    }
    

}
