package com.cos.blog.repository;

import com.cos.blog.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

//DAO
//자동으로 bean등록이 된다.@Repository 생략 가능
public interface UserRepository extends JpaRepository<User,Integer> { //User테이블을 관리하는 Repository이고 PK는 Integer임

    //SELECT * FROM user WHERE username = 1?;
    Optional<User> findByUsername(String username);

    //JPA Naming 쿼리 전략

    //SELECT*FROM user WHERE username = ?1 AND password = ?2 아래 가 이런 뜻을 가짐 ? <- 여기엔 아래 파라미터 username,password가 들어간다.
//    User findByUsernameAndPassword(String username, String password);

    //위와 같은 방식(네이티브 쿼리 )
//    @Query(value="SELECT*FROM user WHERE username = ?1 AND password = ?2",nativeQuery = true)
//    User login(String username, String password);
}
