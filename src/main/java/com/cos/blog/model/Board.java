package com.cos.blog.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//Auto_increment
    private int id;

    @Column(nullable = false,length = 100)
    private String title;

    @Lob//대용량 데이터
    private String content; // 섬머노트 라이브러리 <html>태그가 섞여서 디자인이 됨

    private int count;// 조회수

    //Board를 select 할 때 자동으로 User 정보를 가져옴 user정보는 하나일 테니까
    @ManyToOne(fetch = FetchType.EAGER) // Many = Board, User= One 한명의 user는 여러개의 게시글을 쓸 수 있다.
    @JoinColumn(name = "userId")
    private User user; //DB는 오브젝트를 저장할 수 없다. FK, 자바는 오브젝트를 저장할 수 있다.

    //Board select 할 때 reply는 엄청 많은 갯수가 될 수 있어서 필요하면 가져오고 아니면 가져오지 않음 (FetchType.LAZY)<- 이게 기본전략임
    @OneToMany(mappedBy = "board",fetch =FetchType.EAGER, cascade = CascadeType.REMOVE) // mappedBy 연관관계의 주인이 아니다( 난 FK가 아니에요) DB에 칼럼을 만들지마라 // 즉 테이블에 생성되는 FK아님
//    @JoinColumn(name="replyid") -> 필요가 없음
    @JsonIgnoreProperties({"board"}) // @@@@@무한 참조 방지@@@@@
    @OrderBy("id desc")// 정렬
    private List<Reply> replys;

    @CreationTimestamp  //데이터가 insert or update될때 자동으로 들어감
    private Timestamp createDate;


}
