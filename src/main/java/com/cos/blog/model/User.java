package com.cos.blog.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.sql.Timestamp;
//ORM -> Java(다른언어) Object -> 테이블로 매핑해주는 기술

@Data //getter sertter
@NoArgsConstructor//빈 생성자
@AllArgsConstructor//전체 생성자
@Builder// builder패턴
@Entity// User 클래스가 MySQL에 자동으로 테이블이 생성이 된다.
//@DynamicInsert //insert할 때 null인 필드는 제외해준다.

/* 스키마 변경시 yml create로 변경 해줘야한다 !!!*/
public class User {
    @Id // Primary key

    @GeneratedValue(strategy = GenerationType.IDENTITY)// 프로젝트에서 연결된 DB넘버링 전략을 따라간다.
    private int id;// 오라클=시퀀스 mysql=auto_increment

    @Column(nullable = false,length = 100, unique = true)
    private String username;// 아이디

    @Column(nullable = false,length = 100) //12345=> 해쉬(비밀번호 암호화)
    private String password;

    @Column(nullable = false,length = 50)
    private String email;

//    @ColumnDefault("user")
    @Enumerated(EnumType.STRING)
    private RoleType role; //Enum을 쓰는게 좋다.// ADMIN,USER  --데이터 타입에 enum으로 넣으면 enum안의 값들만 들어가게 강제된다.

    private String oauth;//kakao,goolet 어디로 로그인 했는지

    @CreationTimestamp //시간이 자동 입력
    private Timestamp createDate;

}
