package com.cos.blog.service;

import com.cos.blog.dto.ReplySaveRequestDto;
import com.cos.blog.model.Board;
import com.cos.blog.model.Reply;
import com.cos.blog.model.RoleType;
import com.cos.blog.model.User;
import com.cos.blog.repository.BoardRepository;
import com.cos.blog.repository.ReplyRepository;
import com.cos.blog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/*
    서비스가 필요한 이유?
    1.트랜잭션 관리
    2.서비스의 의미 때문 ex) 송금서비스 update할 때 -> commit이 일어남 //하나의 서비스, 기능
* */


// 스프링이 컴포넌트 스캔을 통해서 bean에 등록해줌 IoC해준다.
@Service
public class BoardService {

    @Autowired // DI함
    private BoardRepository boardRepository;

    @Autowired // DI함
    private ReplyRepository replyRepository;

    @Autowired // DI함
    private UserRepository userRepository;

    @Transactional//전체가 성공하면 commit 실패하면 Rollback하게 할 수 있음
    public void 글쓰기(Board board , User user) { //title, content
        board.setCount(0);
        board.setUser(user);
        boardRepository.save(board);
    }
    @Transactional(readOnly = true)
    public Page<Board> 글목록(Pageable pageable){
        return boardRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Board 글상세보기(int id){
        return boardRepository.findById(id)
                .orElseThrow(()->{
                    return new IllegalArgumentException("글 상세보기 실패 : 아이디를 찾을 수 없습니다.");
                });

    }
    @Transactional
    public void 글삭제하기(int id) {
        boardRepository.deleteById(id);
    }

    @Transactional
    public void 글수정하기(int id, Board requestBoard) {
        Board board = boardRepository.findById(id)
                .orElseThrow(()->{
                    return new IllegalArgumentException("글 찾기 실패 : 아이디를 찾을 수 없습니다.");
                }); // 영속화 완료
        board.setTitle(requestBoard.getTitle());
        board.setContent(requestBoard.getContent());
        //해당 함수로 종료시에 (Service가 종료될 때)  트랜잭션이 종료. 이때 더티체킹 - 자동 업데이트 DB로 flush
    }
    @Transactional
    public void 댓글쓰기(ReplySaveRequestDto replySaveRequestDto){

//        User user = userRepository.findById(replySaveRequestDto.getUserId())
//                .orElseThrow(()->{
//                    return new IllegalArgumentException("댓글쓰기 실패 : 유저 아이디를 찾을 수 없습니다.");
//                }); // 영속화
//        Board board = boardRepository.findById(replySaveRequestDto.getBoardId())
//                .orElseThrow(()->{
//                    return new IllegalArgumentException("댓글쓰기 실패 : 게시글 아이디를 찾을 수 없습니다.");
//                }); // 영속화 완료;
//
//        Reply reply = Reply.builder()
//                .user(user)
//                .board(board)
//                .content(replySaveRequestDto.getContent())
//                .build();

//        replyRepository.save(reply);//
        //위에는 dto방식 아래는 네이티브 쿼리 방식
        replyRepository.mSave(replySaveRequestDto.getUserId(), replySaveRequestDto.getBoardId(), replySaveRequestDto.getContent());

    }

    @Transactional
    public void 댓글삭제(int replyId) {
        replyRepository.deleteById(replyId);
    }

}
