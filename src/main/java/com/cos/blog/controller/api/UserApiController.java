package com.cos.blog.controller.api;

import com.cos.blog.config.auth.PrincipalDetail;
import com.cos.blog.dto.ResponseDto;
import com.cos.blog.model.RoleType;
import com.cos.blog.model.User;
import com.cos.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class UserApiController {

    @Autowired//DI
    private UserService userService;


    @Autowired
    private AuthenticationManager authenticationManager;
    /** 스프링 시큐리티 적용x 일때  session적용
    @Autowired // 스프링컨테이너가 빈으로 등록해서 가지고있다가 필요하다면 di해서 넣₩어줌
    private HttpSession session;
    **/
    @PostMapping("/auth/joinProc")
    public ResponseDto<Integer> save(@RequestBody User user){ //username,password,email
        System.out.println("UserApiController.save");
         userService.회원가입(user);
        return new ResponseDto<Integer>(HttpStatus.OK.value(),1); //자바오브젝트를 json으로 변환해서 리턴(JASON이해줌)
    }

    @PutMapping("/user")
    public ResponseDto<Integer> update(@RequestBody User user){ //key=value,x-www-form-urlencoded
        userService.회원수정(user);
        // 여기서는 트랜잭션이 종료되기 때문에 DB에 값이 변경이 됨
        // 하지만 세션값은 변경되지 않은 상태 직접 세션값을 변경할 것임
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication); // 세션을 강제로 바꿔주는 부분
        return new ResponseDto<Integer>(HttpStatus.OK.value(), 1); //자바오브젝트를 json으로 변환해서 리턴(JASON이해줌)
    }

    /** 스프링 시큐리티 적용 x
    @PostMapping("/api/user/login")
    public ResponseDto<Integer> login(@RequestBody User user) {
        System.out.println("UserApiController.login");
        User principal = userService.로그인(user); //principa (접근주체)

        if(principal != null){
            session.setAttribute("principal",principal);
        }
        return new ResponseDto<Integer>(HttpStatus.OK.value(),1); //자바오브젝트를 json으로 변환해서 리턴(JASON이해줌)
    }
*/
}
